module Day10
  ( day
  , parseMap
  , AsteroidMap(..)
  , visibleFrom
  , findBestMonitoring
  , laserOrder
  )
where

import           Control.Monad                  ( join )
import           Data.Bifunctor
import           Data.Function
import           Data.Ratio
import           Day                            ( Day(Day) )
import qualified Data.List                     as List
import           Data.Text                      ( Text )
import qualified Data.Text                     as Text
import           Data.Set                       ( Set
                                                , (\\)
                                                )
import qualified Data.Set                      as Set

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . findBestMonitoring . parseMap

challengeTwo :: Text -> Text
challengeTwo input =
  let map     = parseMap input
      monitor = fst $ findBestMonitoring map
      result  = laserOrder monitor (Set.delete monitor $ asteroids map) !! 199
  in  Text.pack . show $ result

data Dir
  = N
  | NE
  | E
  | SE
  | S
  | SW
  | W
  | NW
  deriving (Show, Eq, Ord, Enum, Bounded)

newtype Angle
  = Angle (Integer, Integer)
  deriving (Show, Eq)

mkAngle :: Coord -> Coord -> Angle
mkAngle (fx, fy) (tx, ty) = case (tx - fx, ty - fy) of
  (0 , dy) -> Angle (0, signum dy)
  (dx, 0 ) -> Angle (signum dx, 0)
  (dx, dy) -> let fac = gcd dx dy in Angle (dx `div` fac, dy `div` fac)

angleDir :: Angle -> Dir
angleDir (Angle (x, y)) = case (signum x, signum y) of
  (0 , -1) -> N
  (1 , -1) -> NE
  (1 , 0 ) -> E
  (1 , 1 ) -> SE
  (0 , 1 ) -> S
  (-1, 1 ) -> SW
  (-1, 0 ) -> W
  (-1, -1) -> NW
  _        -> error "invalid angle"

instance Ord Angle where
  compare a b = case compare (angleDir a) (angleDir b) of
    EQ -> case angleDir a of
      N -> EQ
      E -> EQ
      S -> EQ
      W -> EQ
      _ -> angleRatio b `compare` angleRatio a
    other -> other

   where
    angleRatio :: Angle -> Rational
    angleRatio (Angle (x, y)) = x % y

type Coord = (Integer, Integer)

data AsteroidMap
  = AsteroidMap
  { width :: Integer
  , height :: Integer
  , asteroids :: Set Coord
  }
  deriving (Show, Eq)

parseMap :: Text -> AsteroidMap
parseMap input =
  let rows      = Text.lines input
      height    = fromIntegral $ length rows
      width     = fromIntegral $ Text.length (head rows)
      asteroids = Set.fromList
        [ (x, y)
        | (y, row ) <- zip [0 ..] rows
        , (x, cell) <- zip [0 ..] (Text.unpack row)
        , cell == '#'
        ]
  in  AsteroidMap { .. }

visibleFrom :: AsteroidMap -> Coord -> Set Coord
visibleFrom AsteroidMap {..} from@(x, y) =
  let others = Set.delete from asteroids
  in  others \\ Set.unions
        (fmap (\other -> occlusion width height from other) $ Set.toList others)

occlusion :: Integer -> Integer -> Coord -> Coord -> Set Coord
occlusion width height from@(fx, fy) behind@(bx, by) =
  let (dx, dy) = case (bx - fx, by - fy) of
        (0 , dy) -> (0, signum dy)
        (dx, 0 ) -> (signum dx, 0)
        (dx, dy) -> let fac = gcd dx dy in (dx `div` fac, dy `div` fac)
  in  Set.fromList $ takeWhile
        (\(x, y) -> x >= 0 && y >= 0 && x < width && y < height)
        [ (bx + n * dx, by + n * dy) | n <- [1 ..] ]

findBestMonitoring :: AsteroidMap -> (Coord, Int)
findBestMonitoring map = List.maximumBy (compare `on` snd) $ fmap
  (\a -> (a, Set.size (visibleFrom map a)))
  (Set.toList $ asteroids map)

manhattan :: Coord -> Coord -> Integer
manhattan (ax, ay) (bx, by) = abs (bx - ax) + abs (by - ay)

laserOrder :: Coord -> Set Coord -> [Coord]
laserOrder from targets =
  let targetsByAngle =
          fmap (fmap fst)
            . List.groupBy ((==) `on` snd)
            . List.sortOn snd
            . fmap (\a -> (a, mkAngle from a))
            $ Set.toList targets
      targetsByAngleAndDistance =
          fmap (List.sortOn (manhattan from)) targetsByAngle
  in  join . List.transpose $ targetsByAngleAndDistance
