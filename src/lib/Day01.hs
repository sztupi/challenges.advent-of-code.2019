module Day01
  ( day
  , moduleFuelReq
  , moduleFuelReqRec
  )
where

import           Day                            ( Day(Day) )
import           Data.Text                      ( Text )
import           Data.Either
import qualified Data.Text                     as Text
import qualified Data.Text.Read                as Text

day :: Day
day = Day challengeOne challengeTwo

moduleFuelReqRec :: Int -> Int
moduleFuelReqRec = sum . takeWhile (>= 0) . drop 1 . iterate moduleFuelReq

moduleFuelReq :: Int -> Int
moduleFuelReq n = n `div` 3 - 2

modules :: Text -> [Int]
modules = fmap fst . rights . fmap Text.decimal . Text.lines

challengeOne :: Text -> Text
challengeOne = Text.pack . show . sum . fmap moduleFuelReq . modules

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . sum . fmap moduleFuelReqRec . modules

