module IntCode
  ( Program
  , mkProgram
  , parse
  , adjust
  , run
  , Machine
  , chainMachine
  , loopMachine
  , runMachine
  , runSingleProgram
  , RunState(..)
  , step
  , mkStProgram
  )
where

import           Prelude                 hiding ( read )
import           Control.Monad.Fix
import           Control.Monad.ST               ( ST )
import qualified Control.Monad.ST              as ST
import           Data.STRef                     ( STRef )
import qualified Data.STRef                    as ST
import           Debug.Trace
import           Data.Maybe
import           Data.Function
import           Data.Either
import           Data.Foldable
import qualified Data.Map.Strict               as Map
import           Data.Map                       ( Map )
import           Data.Text                      ( Text )
import qualified Data.Text                     as Text
import qualified Data.Text.Read                as Text
import           Data.Array                     ( Array
                                                , (!)
                                                , (//)
                                                )
import qualified Data.Array                    as A
import           Control.Monad.RWS              ( RWS )
import qualified Control.Monad.RWS             as RWS
import qualified Data.Sequence                 as Seq
import           Data.Sequence                  ( Seq
                                                , (|>)
                                                , ViewL(..)
                                                )

type Program = Array Integer Integer
type Input = [Integer]
type Output = Seq.ViewL Integer

data ProgramState
  = ProgramState
  { pstMemory :: Map Integer Integer
  , pstPC :: Integer
  , pstRelBase :: Integer
  }

data RunState
  = RSFinished ProgramState
  | RSOutput Integer RunState
  | RSInput (Integer -> RunState)

mkProgram :: [Integer] -> IntCode.Program
mkProgram program =
  A.listArray (0, fromIntegral $ length program - 1) $ program

mkStProgram :: Program -> ProgramState
mkStProgram prog =
  let mem = Map.fromList $ zip [0 ..] $ toList prog
  in  ProgramState { pstMemory = mem, pstPC = 0, pstRelBase = 0 }

parse :: Text -> Program
parse input =
  let list =
          fmap fst
            . rights
            . fmap (Text.signed Text.decimal)
            . Text.splitOn ","
            $ input
  in  A.listArray (0, fromIntegral $ length list - 1) list

adjust :: [(Integer, Integer)] -> Program -> Program
adjust adjustments program = program // adjustments


type Instruction = Integer

data ParamMode
  = Position
  | Immediate
  | Relative
type ParamModes = Integer -> ParamMode

parseOpCode :: Integer -> (Instruction, ParamModes)
parseOpCode opcode =
  let (modes, inst) = opcode `divMod` 100 in (inst, lookupParamMode modes)

lookupParamMode :: Integer -> Integer -> ParamMode
lookupParamMode modes pc = case (modes `div` (10 ^ (pc - 1))) `mod` 10 of
  0 -> Position
  1 -> Immediate
  2 -> Relative

step :: ProgramState -> RunState
step prog = do
  let opcode = fromMaybe 0 . Map.lookup (pstPC prog) . pstMemory $ prog
  case parseOpCode opcode of
    (1 , pm) -> add pm prog
    (2 , pm) -> mult pm prog
    (3 , pm) -> input pm prog
    (4 , pm) -> output pm prog
    (5 , pm) -> jumpIfTrue pm prog
    (6 , pm) -> jumpIfFalse pm prog
    (7 , pm) -> storeLessThan pm prog
    (8 , pm) -> storeEqual pm prog
    (9 , pm) -> adjustRelativeBase pm prog
    (99, _ ) -> RSFinished prog

modifyPC :: (Integer -> Integer) -> ProgramState -> ProgramState
modifyPC f p = p { pstPC = f $ pstPC p }

modifyRelBase :: (Integer -> Integer) -> ProgramState -> ProgramState
modifyRelBase f p = p { pstRelBase = f $ pstRelBase p }

input :: ParamModes -> ProgramState -> RunState
input pm prog = RSInput $ \nextInput ->
  step (prog & write pm (pstPC prog) 1 nextInput & modifyPC (+ 2))

output :: ParamModes -> ProgramState -> RunState
output pm prog =
  let value = read pm (pstPC prog) 1 prog
  in  RSOutput value $ step (prog & modifyPC (+ 2))

jumpIf :: (Integer -> Bool) -> ParamModes -> ProgramState -> RunState
jumpIf pred pm prog =
  let value = read pm (pstPC prog) 1 prog
  in  if pred value
        then
          let addr = read pm (pstPC prog) 2 prog
          in  step (prog & modifyPC (const addr))
        else step (prog & modifyPC (+ 3))

jumpIfTrue :: ParamModes -> ProgramState -> RunState
jumpIfTrue = jumpIf (/= 0)

jumpIfFalse :: ParamModes -> ProgramState -> RunState
jumpIfFalse = jumpIf (== 0)

storeCompare
  :: (Integer -> Integer -> Bool) -> ParamModes -> ProgramState -> RunState
storeCompare comp pm prog =
  let v1     = read pm (pstPC prog) 1 prog
      v2     = read pm (pstPC prog) 2 prog
      result = if comp v1 v2 then 1 else 0
  in  step (prog & write pm (pstPC prog) 3 result & modifyPC (+ 4))

storeLessThan :: ParamModes -> ProgramState -> RunState
storeLessThan = storeCompare (<)

storeEqual :: ParamModes -> ProgramState -> RunState
storeEqual = storeCompare (==)

adjustRelativeBase :: ParamModes -> ProgramState -> RunState
adjustRelativeBase pm prog =
  let v = read pm (pstPC prog) 1 prog
  in  step (prog & modifyRelBase (+ v) & modifyPC (+ 2))

readValue :: Integer -> Integer -> ProgramState -> Integer
readValue addr offset prog =
  fromMaybe 0 $ Map.lookup (addr + offset) (pstMemory prog)

writeValue :: Integer -> Integer -> Integer -> ProgramState -> ProgramState
writeValue addr offset value prog =
  prog { pstMemory = Map.insert (addr + offset) value (pstMemory prog) }

read :: ParamModes -> Integer -> Integer -> ProgramState -> Integer
read pm addr offset prog = case pm offset of
  Position  -> readFromAddr addr offset prog
  Immediate -> readValue addr offset prog
  Relative ->
    let relBase    = pstRelBase prog
        paramValue = readValue addr offset prog
    in  readValue (relBase + paramValue) 0 prog

readFromAddr :: Integer -> Integer -> ProgramState -> Integer
readFromAddr addr offset prog =
  let readAddr = readValue addr offset prog in readValue readAddr 0 prog

write
  :: ParamModes -> Integer -> Integer -> Integer -> ProgramState -> ProgramState
write pm addr offset value prog = case pm offset of
  Position  -> writeToAddr addr offset value prog
  Immediate -> error "can't write to immediate"
  Relative ->
    let relBase    = pstRelBase prog
        paramValue = readValue addr offset prog
    in  writeValue (relBase + paramValue) 0 value prog

writeToAddr :: Integer -> Integer -> Integer -> ProgramState -> ProgramState
writeToAddr addr offset value prog =
  let writeAddr = readValue addr offset prog
  in  prog & writeValue writeAddr 0 value

binaryOp
  :: (Integer -> Integer -> Integer) -> ParamModes -> ProgramState -> RunState
binaryOp op pm prog =
  let a = read pm (pstPC prog) 1 prog
      b = read pm (pstPC prog) 2 prog
  in  step (prog & write pm (pstPC prog) 3 (a `op` b) & modifyPC (+ 4))

add :: ParamModes -> ProgramState -> RunState
add = binaryOp (+)

mult :: ParamModes -> ProgramState -> RunState
mult = binaryOp (*)


-- Running

data Pull
  = PullValue Integer
  | PullEOF

type PullSource s = ST s Pull
type PullFactory s = ST s (PullSource s)

pullFromStatic :: [Integer] -> forall s . PullFactory s
pullFromStatic values = do
  seq <- ST.newSTRef $ Seq.fromList values
  pullFromSTQueue seq

pullFromSTQueue :: forall s . STRef s (Seq Integer) -> PullFactory s
pullFromSTQueue seq = return $ do
  currSeq <- ST.readSTRef seq
  case Seq.viewl currSeq of
    Seq.EmptyL -> return PullEOF
    a :< as    -> do
      ST.writeSTRef seq as
      return $ PullValue a

consume
  :: forall s
   . STRef s (Seq Integer)
  -> PullSource s
  -> RunState
  -> ST s RunState
consume buffer pull = consume'
 where
  consume' :: RunState -> ST s RunState
  consume' = \case
    RSFinished endState -> do
      values <- toList <$> ST.readSTRef buffer
      ST.writeSTRef buffer Seq.empty
      return $ foldl (\k v -> RSOutput v k) (RSFinished endState) values
    RSOutput value cont -> do
      ST.modifySTRef' buffer (|> value)
      consume' cont
    RSInput kont -> pull >>= \case
      PullValue value -> consume' $ kont value
      PullEOF         -> do
        values <- toList <$> ST.readSTRef buffer
        ST.writeSTRef buffer Seq.empty
        return $ foldl (\k v -> RSOutput v k) (RSInput kont) values

data CollectResult s
  = ProgramFinished [Integer] ProgramState
  | ProgramBlockedOnInput [Integer] (Integer -> ST s RunState)

collectOutputs
  :: forall s . STRef s (Seq Integer) -> ST s RunState -> ST s (CollectResult s)
collectOutputs buffer runProgram = runProgram >>= collectOutputs'
 where
  collectOutputs' = \case
    RSFinished endState -> do
      values <- reverse . toList <$> ST.readSTRef buffer
      return $ ProgramFinished values endState
    RSOutput value cont -> do
      ST.modifySTRef' buffer (|> value)
      collectOutputs' cont
    RSInput kont -> do
      values <- reverse . toList <$> ST.readSTRef buffer
      return $ ProgramBlockedOnInput values (return . kont)


run :: (Program, Input) -> (Program, Output)
run (memory, input) = ST.runST $ do
  let stProg = mkStProgram memory
  pull   <- pullFromStatic input
  buffer <- ST.newSTRef (Seq.fromList [])
  result <- collectOutputs buffer (consume buffer pull $ step stProg)
  case result of
    ProgramFinished output endState -> do
      let endMemory = pstMemory endState
          memArray =
            A.array
                (fst . Map.findMin $ endMemory, fst . Map.findMax $ endMemory)
              $ Map.toList
              $ endMemory
      return (memArray, Seq.viewl (Seq.fromList output))
    ProgramBlockedOnInput output cont -> error "program waiting for input"

-- Machines

fuse :: RunState -> RunState -> RunState
fuse runSource runTarget = case runTarget of
  RSFinished endTargetState -> RSFinished endTargetState
  RSOutput value cont       -> RSOutput value (fuse runSource cont)
  RSInput kont              -> case runSource of
    RSFinished endSourceState -> RSInput kont
    RSOutput value innerCont  -> fuse innerCont (kont value)
    RSInput innerKont ->
      RSInput $ \value -> fuse (innerKont value) (RSInput kont)

newtype Machine = Machine (forall s . Input -> ST s (CollectResult s))

chainMachine :: [(Program, Input)] -> Machine
chainMachine programs = Machine $ \input -> do
  let m = foldl1 (\a b -> fuse <$> a <*> b) $ fmap
        (\((prog, input), n) -> do
          let stProg = mkStProgram prog
          pull   <- pullFromStatic input
          buffer <- ST.newSTRef Seq.empty
          consume buffer pull (step stProg)
        )
        (zip programs [0 ..])

  pull   <- pullFromStatic input
  buffer <- ST.newSTRef Seq.empty
  collectOutputs buffer $ m >>= consume buffer pull

loopMachine :: [(Program, Input)] -> Machine
loopMachine programs = Machine $ \input -> do
  parts <- mapM
    (\((prog, input), n) -> do
      let stProg = mkStProgram prog
      pull   <- pullFromStatic input
      buffer <- ST.newSTRef Seq.empty
      return $ consume buffer pull (step stProg)
    )
    (zip programs [0 ..])
  let m = foldl1 (\a b -> fuse <$> a <*> b) parts
  buffer <- ST.newSTRef (Seq.fromList input)
  pull   <- pullFromSTQueue buffer
  collectOutputs buffer $ m >>= consume buffer pull

runMachine :: Input -> Machine -> Output
runMachine input (Machine machine) = ST.runST $ do
  result <- machine input
  case result of
    ProgramFinished output endState -> return $ Seq.viewl (Seq.fromList output)
    ProgramBlockedOnInput output cont ->
      error $ "program waiting for input, output so far: " <> show output
  -- foldl' runSingleProgram (Seq.viewl (Seq.fromList input)) programs

runSingleProgram :: Output -> (Program, Input) -> Output
runSingleProgram prevOutput (program, input) =
  let input'             = input <> toList prevOutput
      (program', output) = run (program, input')
  in  output
