module Day11
  ( day
  )
where

import           Day                            ( Day(Day) )
import           Data.Maybe
import qualified Data.List                     as List
import           Data.Text                      ( Text )
import qualified Data.Text                     as Text
import           Data.Foldable
import           Data.Set                       ( Set
                                                , (\\)
                                                )
import qualified Data.Set                      as Set
import qualified IntCode
import           Data.Map                       ( Map )
import qualified Data.Map.Strict               as Map

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne programText =
  let program   = IntCode.parse $ Text.strip programText
      robot     = mkRobot program
      map       = Map.empty
      (_, map') = runRobotOnMap map robot
  in  Text.pack . show $ Map.size map'

challengeTwo :: Text -> Text
challengeTwo programText =
  let program   = IntCode.parse $ Text.strip programText
      robot     = mkRobot program
      map       = Map.insert (0, 0) White Map.empty
      (_, map') = runRobotOnMap map robot
  in  paintMap map'

data Direction
  = DUp
  | DLeft
  | DRight
  | DDown

type Pos = (Integer, Integer)

data Robot
  = Robot
  { rRunState :: IntCode.RunState
  , rDir :: Direction
  , rPos :: Pos
  }

data Color
  = White
  | Black

colorValue :: Color -> Integer
colorValue White = 1
colorValue Black = 0

toColor :: Integer -> Color
toColor 0 = Black
toColor 1 = White
toColor c = error $ "invalid color " <> show c

mkRobot :: IntCode.Program -> Robot
mkRobot prog = Robot { rRunState = IntCode.step (IntCode.mkStProgram prog)
                     , rDir      = DUp
                     , rPos      = (0, 0)
                     }

turn :: Integer -> Direction -> Direction
turn 0 DUp    = DLeft
turn 0 DLeft  = DDown
turn 0 DDown  = DRight
turn 0 DRight = DUp
turn 1 DLeft  = DUp
turn 1 DDown  = DLeft
turn 1 DRight = DDown
turn 1 DUp    = DRight

move :: Direction -> Pos -> Pos
move DUp    (x, y) = (x, y - 1)
move DLeft  (x, y) = (x - 1, y)
move DDown  (x, y) = (x, y + 1)
move DRight (x, y) = (x + 1, y)

step :: Robot -> Color -> Maybe (Robot, Color)
step robot color = case rRunState robot of
  (IntCode.RSInput kont) ->
    let (IntCode.RSOutput paintCode (IntCode.RSOutput turnCode runState')) =
            kont (colorValue color)
        dir' = turn turnCode (rDir robot)
        pos' = move dir' (rPos robot)
    in  Just
          ( robot { rRunState = runState', rDir = dir', rPos = pos' }
          , toColor paintCode
          )
  (IntCode.RSFinished _) -> Nothing
  _                      -> error "unexpected robot state"

stepMap :: Map Pos Color -> Robot -> Maybe (Robot, Map Pos Color)
stepMap map robot = do
  let pos   = rPos robot
      color = fromMaybe Black $ Map.lookup pos map
  (robot', color') <- step robot color
  let map' = Map.insert pos color' map
  return (robot', map')

runRobotOnMap :: Map Pos Color -> Robot -> (Robot, Map Pos Color)
runRobotOnMap map robot = case stepMap map robot of
  Nothing             -> (robot, map)
  Just (robot', map') -> runRobotOnMap map' robot'

paintMap :: Map Pos Color -> Text
paintMap map =
  let keys                     = Map.keys map
      (minX, maxX, minY, maxY) = foldl
        (\(a, b, c, d) (x, y) -> (min a x, max b x, min c y, max d y))
        (10 ^ 8, -(10 ^ 8), 10 ^ 8, -(10 ^ 8))
        keys
  in  Text.intercalate
        "\n"
        [ line
        | y <- [minY .. maxY]
        , let line = Text.pack
                [ c
                | x <- [minX .. maxX]
                , let c = colorChar $ fromMaybe Black $ Map.lookup (x, y) map
                ]
        ]

colorChar :: Color -> Char
colorChar White = '#'
colorChar Black = ' '
