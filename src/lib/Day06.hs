module Day06
  ( day
  , parseOrbits
  , directOrbits
  , indirectOrbits
  , allOrbits
  , orbitsFromDirects
  )
where

import           Control.Monad                  ( join )
import           Data.Bifunctor
import           Day                            ( Day(Day) )
import           Data.Text                      ( Text )
import qualified Data.Text                     as Text
import           Data.Set                       ( Set
                                                , (\\)
                                                )
import qualified Data.Set                      as Set

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne input =
  let orbits = parseOrbits input
  in  Text.pack . show . Set.size $ allOrbits orbits

challengeTwo :: Text -> Text
challengeTwo input =
  let orbits    = parseOrbits input
      youOrbits = bodyOrbits orbits "YOU"
      sanOrbits = bodyOrbits orbits "SAN"
      common    = fmap fst $ takeWhile (uncurry (==)) $ zip youOrbits sanOrbits
      y         = length youOrbits
      s         = length sanOrbits
      c         = length common
  in  Text.pack . show $ y - c + s - c

type Body = Text

data Orbits
  = Orbits
  { directOrbits :: Set (Body, Body)
  }

orbitsFromDirects :: [(Body, Body)] -> Orbits
orbitsFromDirects = Orbits . Set.fromList

parseOrbits :: Text -> Orbits
parseOrbits =
  orbitsFromDirects
    . fmap (second (Text.drop 1) . Text.breakOn ")" . Text.strip)
    . Text.lines

indirectOrbits :: Orbits -> Set (Body, Body)
indirectOrbits o = allOrbits o \\ directOrbits o

allOrbits :: Orbits -> Set (Body, Body)
allOrbits o =
  Set.fromList
    $ join
    $ fmap (\b -> (, b) <$> bodyOrbits o b)
    $ Set.toList
    $ bodies o

bodies :: Orbits -> Set Body
bodies (Orbits directs) =
  (Set.map fst directs) `Set.union` (Set.map snd directs)

bodyOrbits :: Orbits -> Body -> [Body]
bodyOrbits orbits@(Orbits directs) = reverse . bodyOrbits'
 where
  bodyOrbits' :: Body -> [Body]
  bodyOrbits' body =
    case map fst . filter ((== body) . snd) . Set.toList $ directs of
      [parent] -> parent : bodyOrbits' parent
      []       -> []
      parents  -> error $ "body has multiple parents: " <> show parents
