module Day07
  ( day
  )
where

import           Day                            ( Day(Day) )
import           Data.Text                      ( Text )
import qualified Data.Text                     as Text
import qualified IntCode
import           IntCode                        ( chainMachine
                                                , loopMachine
                                                )
import           Data.Foldable
import           Data.Functor
import           Data.Bifunctor
import           Data.Function
import           Data.List

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne programText =
  let
    program = IntCode.parse $ Text.strip programText
    perms = permutations [0, 1, 2, 3, 4]
    machines =
      (\perm -> (perm, chainMachine ((program, ) . (: []) <$> perm))) <$> perms
    results = second (head . toList . IntCode.runMachine [0]) <$> machines
    result  = maximumBy (compare `on` snd) results
  in
    Text.pack . show $ result


challengeTwo :: Text -> Text
challengeTwo programText =
  let
    program = IntCode.parse $ Text.strip programText
    perms = permutations [5, 6, 7, 8, 9]
    machines =
      (\perm -> (perm, loopMachine ((program, ) . (: []) <$> perm))) <$> perms
    results = second (head . toList . IntCode.runMachine [0]) <$> machines
    result  = maximumBy (compare `on` snd) results
  in
    Text.pack . show $ result

