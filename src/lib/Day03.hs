module Day03
  ( day
  , WireDirection(..)
  , WireSegment(..)
  , WireDesc
  , Wire
  , Pos
  , SimpleWire
  , TimingSensitiveWire
  , parseWireText
  , closestIntersectionDistance
  , tracePoints
  , findIntersections
  )
where

import           Data.Functor                   ( ($>) )
import           Data.Maybe
import           Day                            ( Day(Day) )
import qualified Data.List                     as List
import           Data.Map                       ( Map )
import qualified Data.Map                      as Map
import           Data.Set                       ( Set )
import qualified Data.Set                      as Set
import           Data.Text                      ( Text )
import qualified Data.Text                     as Text
import           Data.Either.Extra
import           Text.Megaparsec         hiding ( Pos )
import           Text.Megaparsec.Char
import           Text.Megaparsec.Char.Lexer
import           Data.Void                      ( Void )

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = challenge @SimpleWire

challengeTwo :: Text -> Text
challengeTwo = challenge @TimingSensitiveWire

challenge :: forall  w . Wire w => Text -> Text
challenge input = case parseWireText <$> Text.lines input of
  [Right a, Right b] -> Text.pack . show $ closestIntersectionDistance @w a b
  e                  -> error $ "invalid input: " <> show e

parseWireText :: Text -> Either String WireDesc
parseWireText input = mapLeft show $ runParser wireP "<none>" input

type WireParser = Parsec Void Text

wireP :: WireParser WireDesc
wireP = segmentP `sepBy` "," <* eof

segmentP :: WireParser WireSegment
segmentP = WireSegment <$> directionP <*> decimal

directionP :: WireParser WireDirection
directionP = choice
  [ char 'L' $> WDLeft
  , char 'R' $> WDRight
  , char 'U' $> WDUp
  , char 'D' $> WDDown
  ]

data WireDirection
  = WDLeft
  | WDRight
  | WDUp
  | WDDown
  deriving (Eq, Show, Enum, Bounded)

data WireSegment
  = WireSegment WireDirection Int
  deriving (Eq, Show)

type WireDesc = [WireSegment]

type Pos = (Int, Int)

class Wire w where
  fromPosList :: [Pos] -> w
  intersectionValue :: w -> w -> Pos -> Maybe Int

newtype SimpleWire = SimpleWire (Set Pos)
  deriving (Show)

instance Wire SimpleWire where
  fromPosList = SimpleWire . Set.fromList

  intersectionValue (SimpleWire w1) (SimpleWire w2) p@(x, y) =
    if p `Set.member` w1 && p `Set.member` w2
      then Just (abs x + abs y)
      else Nothing

newtype TimingSensitiveWire = TimingSensitiveWire (Map Pos Int)
  deriving (Show)

instance Wire TimingSensitiveWire where
  fromPosList ps = TimingSensitiveWire $ Map.fromList $ zip ps [1 ..]

  intersectionValue (TimingSensitiveWire w1) (TimingSensitiveWire w2) p = do
    d1 <- Map.lookup p w1
    d2 <- Map.lookup p w2
    return $ d1 + d2

closestIntersectionDistance
  :: forall  w . Wire w => WireDesc -> WireDesc -> Maybe Int
closestIntersectionDistance a b =
  let pointsA       = tracePoints a
      pointsB       = tracePoints b
      intersections = findIntersections pointsA pointsB
      wa            = fromPosList @w pointsA
      wb            = fromPosList @w pointsB
  in  Set.findMin $ Set.map (intersectionValue wa wb) intersections

findIntersections :: [Pos] -> [Pos] -> Set Pos
findIntersections a b = Set.fromList a `Set.intersection` Set.fromList b

tracePoints :: WireDesc -> [Pos]
tracePoints = snd . foldl traceSegment ((0, 0), [])
 where
  traceSegment :: (Pos, [Pos]) -> WireSegment -> (Pos, [Pos])
  traceSegment (pos, points) (WireSegment dir len) =
    let newPoints = drop 1 . take (len + 1) . iterate (step dir) $ pos
    in  (last newPoints, points <> newPoints)

  step :: WireDirection -> Pos -> Pos
  step WDLeft  (x, y) = (x - 1, y)
  step WDRight (x, y) = (x + 1, y)
  step WDUp    (x, y) = (x, y - 1)
  step WDDown  (x, y) = (x, y + 1)
