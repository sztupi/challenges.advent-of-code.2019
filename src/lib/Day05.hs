module Day05
  ( day
  )
where

import           Day                            ( Day(Day) )
import           Data.Text                      ( Text )
import qualified Data.Text                     as Text
import qualified IntCode                       as IntCode
import           Data.Foldable

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne programText =
  let program = IntCode.parse $ Text.strip programText
      result  = IntCode.run (program, [1])
  in  Text.pack . show . toList $ snd result

challengeTwo :: Text -> Text
challengeTwo programText =
  let program = IntCode.parse $ Text.strip programText
      result  = IntCode.run (program, [5])
  in  Text.pack . show . toList $ snd result
