module Day08
  ( day
  , chunksOf
  )
where

import           Day                            ( Day(Day) )
import           Data.Text                      ( Text )
import qualified Data.Text                     as Text
import           Data.Foldable
import           Data.Functor
import           Data.Bifunctor
import           Data.Function
import qualified Data.List                     as List
import           Data.Vector                    ( Vector )
import qualified Data.Vector                   as V
import           Data.Char

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne input =
  let layers    = parseLayers 25 6 (Text.strip input)
      min0Layer = List.minimumBy (compare `on` countDigits 0) layers
  in  Text.pack . show $ countDigits 1 min0Layer * countDigits 2 min0Layer

challengeTwo :: Text -> Text
challengeTwo input =
  let layers      = parseLayers 25 6 (Text.strip input)
      mergedLayer = foldl1 mergeLayers layers
  in  renderLayer mergedLayer

type Layer = Vector (Vector Int)

mkLayer :: [[Int]] -> Layer
mkLayer rows = V.fromList $ fmap V.fromList rows

renderLayer :: Layer -> Text
renderLayer = Text.intercalate "\n" . V.toList . V.map
  (Text.map (\case '0' -> ' '; '1' -> '*'; o -> o) . Text.pack . V.toList . V.map intToDigit)

mergeLayers :: Layer -> Layer -> Layer
mergeLayers top bottom =
  V.map
      (V.map
        (\case
          (2, b) -> b
          (t, _) -> t
        )
      )
    $ V.map (uncurry V.zip)
    $ V.zip top bottom

countDigits :: Int -> Layer -> Int
countDigits digit = sum . V.map (V.length . V.filter (== digit))

chunksOf :: Int -> [a] -> [[a]]
chunksOf _ [] = []
chunksOf n l  = take n l : chunksOf n (drop n l)

parseLayers :: Int -> Int -> Text -> [Layer]
parseLayers w h =
  fmap (mkLayer . fmap (fmap digitToInt . Text.unpack))
    . chunksOf h
    . Text.chunksOf w
