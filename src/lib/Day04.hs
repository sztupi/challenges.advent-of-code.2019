module Day04
  ( day
  , digits
  )
where

import           Day                            ( Day(Day) )
import           Data.Text                      ( Text )
import qualified Data.Text                     as Text

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne input =
  let (a, b) = parseInput input
  in  Text.pack . show . length $ findSolutions isSolutionOne a b

challengeTwo :: Text -> Text
challengeTwo input =
  let (a, b) = parseInput input
  in  Text.pack . show . length $ findSolutions isSolutionTwo a b

parseInput :: Text -> (Int, Int)
parseInput input = case Text.splitOn "-" input of
  [a, b] -> (read . Text.unpack $ a, read . Text.unpack $ b)
  o      -> error $ "invalid input " <> show input

findSolutions :: (Int -> Bool) -> Int -> Int -> [Int]
findSolutions isSolution a b = [ n | n <- [a .. b], isSolution n ]

-- brittany-disable-next-bindings
isSolutionOne :: Int -> Bool
isSolutionOne n =
  let ds    = digits n
      pairs = zip ds (drop 1 ds)
  in  length ds == 6
      && any (\(d1, d2) -> d1 == d2) pairs
      && all (\(d1, d2) -> d1 <= d2) pairs

-- brittany-disable-next-bindings
isSolutionTwo :: Int -> Bool
isSolutionTwo n =
  let ds    = digits n
      pairs = zip ds (drop 1 ds)
  in  length ds == 6
      && any (\(d1, d2) -> d1 == d2) pairs
      && all (\(d1, d2) -> d1 <= d2) pairs
      && 2 `elem` (length <$> groups ds)

groups :: Eq a => [a] -> [[a]]
groups [] = []
groups [x] = [[x]]
groups (x:xs) = (x : takeWhile (== x) xs) : groups (dropWhile (== x) xs)


digits :: Int -> [Int]
digits = reverse . digits'
 where
  digits' :: Int -> [Int]
  digits' 0 = []
  digits' n = n `mod` 10 : digits' (n `div` 10)
