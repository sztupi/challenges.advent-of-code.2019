module Day02
  ( day
  )
where

import           Day                            ( Day(Day) )
import           Data.Text                      ( Text )
import           Data.Either
import qualified Data.Text                     as Text
import           Data.Array                     ( (!) )

import           IntCode                        ( Program )
import qualified IntCode

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne programText =
  let program = IntCode.parse $ Text.strip programText
      result  = IntCode.run (IntCode.adjust [(1, 12), (2, 2)] program, [])
  in  Text.pack . show $ (fst result) ! 0

challengeTwo :: Text -> Text
challengeTwo programText =
  let program      = IntCode.parse $ Text.strip programText
      (noun, verb) = findInputsForOutput 19690720 program
  in  Text.pack . show $ 100 * noun + verb

findInputsForOutput :: Integer -> Program -> (Integer, Integer)
findInputsForOutput output program =
  let programResults = do
        noun <- [0 .. 99]
        verb <- [0 .. 99]
        let resultState =
              IntCode.run (IntCode.adjust [(1, noun), (2, verb)] program, [])
        return ((noun, verb), (fst resultState) ! 0)
  in  fst . head . filter ((== output) . snd) $ programResults
