module Main where

import           Day
import qualified Day01
import qualified Day02
import qualified Day03
import qualified Day04
import qualified Day05
import qualified Day06
import qualified Day07
import qualified Day08
import qualified Day09
import qualified Day10
import qualified Day11
import           System.Environment            as Env
import qualified Data.Text                     as Text
import qualified Data.Text.IO                  as Text.IO
import           Text.Printf                    ( printf )

main :: IO ()
main = do
  [read -> dayNum, read -> challengeNum] <- Env.getArgs
  let day = case dayNum :: Int of
        1  -> Day01.day
        2  -> Day02.day
        3  -> Day03.day
        4  -> Day04.day
        5  -> Day05.day
        6  -> Day06.day
        7  -> Day07.day
        8  -> Day08.day
        9  -> Day09.day
        10 -> Day10.day
        11 -> Day11.day
        _  -> error "nope, no such day yet"

      challenge = case challengeNum of
        1 -> challengeOne day
        2 -> challengeTwo day
        _ -> error "nope, no such challenge on that day"

      inputFileName = printf "inputs/day%02d.txt" dayNum

  input <- Text.IO.readFile inputFileName

  Text.IO.putStrLn $ challenge input



