# Advent of Code 2019
# build / run / test

Using `haskell-stack`:

    $ stack build

    $ stack test

    $ stack exec aoc2019 <day> <challenge>

    e.g.

    $ stack exec aoc2019 2 1
