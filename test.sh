#!/usr/bin/env bash

solved=11

function run_all() {
  for (( i = 1 ; i <= $solved ; i++ )); do
    for j in 1 2; do
      echo "day $i ch $j"
      stack exec aoc2019 $i $j
    done
  done
}

function gen_solution() {
  run_all > solutions.txt
}

function test() {
  diff -u solutions.txt <(run_all)
}
