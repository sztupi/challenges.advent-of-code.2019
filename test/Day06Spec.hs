{-# LANGUAGE QuasiQuotes #-}
module Day06Spec
  ( spec )
where

import Test.Hspec
import Text.InterpolatedString.Perl6 (q)
import qualified Day06
import qualified Data.Set as Set

-- brittany-disable-next-bindings
spec :: Spec
spec = 
  describe "day06" $ 
    describe "parsing" $ do
      it "example" $ do
        let input = 
              [q|COM)B
                 B)C
                 C)D
                 D)E
                 E)F
                 B)G
                 G)H
                 D)I
                 E)J
                 J)K
                 K)L|]

        Day06.directOrbits (Day06.parseOrbits input)
        `shouldBe`
        Set.fromList
          [ ("COM","B")
          , ("B","C")
          , ("C","D")
          , ("D","E")
          , ("E","F")
          , ("B","G")
          , ("G","H")
          , ("D","I")
          , ("E","J")
          , ("J","K")
          , ("K","L")
          ]
      
      it "full example" $ do
        let orbits =   
              Day06.orbitsFromDirects
                [ ("COM","B")
                , ("B","C")
                , ("C","D")
                , ("D","E")
                , ("E","F")
                , ("B","G")
                , ("G","H")
                , ("D","I")
                , ("E","J")
                , ("J","K")
                , ("K","L")
                ]

        Set.size (Day06.allOrbits orbits) `shouldBe` 42
          
