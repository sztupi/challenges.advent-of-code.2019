module Day01Spec
  ( spec
  )
where

import           Test.Hspec
import           Day01

spec :: Spec
spec =
  describe "day01" $          
    describe "examples" $          
      it "example01" $
        moduleFuelReqRec 1969 `shouldBe` 966

