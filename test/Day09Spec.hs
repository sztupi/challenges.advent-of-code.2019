module Day09Spec
( spec )
where

import           Test.Hspec
import qualified IntCode
import           Data.Foldable
import qualified Data.Array                    as A

runProgram :: [Integer] -> [Integer] -> [Integer]
runProgram program input =
  toList . snd . IntCode.run . (, input) . IntCode.mkProgram $ program

-- brittany-disable-next-bindings
spec :: Spec
spec =
  describe "day09" $ 
    describe "examples" $ do
      let prog1 = [ 109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99 ]
      it "example01" $ 
        runProgram prog1 [] `shouldBe` prog1

      let prog2 = [ 1102,34915192,34915192,7,4,7,99,0 ]
      it "example02" $ 
        runProgram prog2 [] `shouldSatisfy` (\[res] -> res >= 10^15 && res < 10^16)

      let prog3 = [ 104,1125899906842624,99 ]
      it "example03" $
        runProgram prog3 [] `shouldBe` [1125899906842624]