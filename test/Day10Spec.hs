{-# LANGUAGE QuasiQuotes #-}
module Day10Spec
  ( spec )
where

import           Test.Hspec
import           Text.InterpolatedString.QM     ( qnb )
import qualified Day10
import           Day10                          ( AsteroidMap(..) )
import qualified Data.Set as Set
import           Data.Set                       ( (\\) )

-- brittany-disable-next-bindings
spec :: Spec
spec = 
  describe "day10" $
    describe "example01" $ do
      let input1 = 
            [qnb|.#..#
                  .....
                  #####
                  ....#
                  ...##|]

          map1 = 
            AsteroidMap
            { width = 5
            , height = 5
            , asteroids = 
              Set.fromList
              [ (1,0), (4,0)
              , (0,2), (1,2), (2,2), (3,2), (4,2)
              , (4,3)
              , (3,4), (4,4)
              ]
            }

          input2 = 
            [qnb|......#.#.
                  #..#.#....
                  ..#######.
                  .#.#.###..
                  .#..#.....
                  ..#....#.#
                  #..#....#.
                  .##.#..###
                  ##...#..#.
                  .#....####|]

          input3 = 
            [qnb|#.#...#.#.
                  .###....#.
                  .#....#...
                  ##.#.#.#.#
                  ....#.#.#.
                  .##..###.#
                  ..#...##..
                  ..##....##
                  ......#...
                  .####.###.|]

          input4 =
            [qnb|.#..#..###
                  ####.###.#
                  ....###.#.
                  ..###.##.#
                  ##.##.#.#.
                  ....###..#
                  ..#.#..#.#
                  #..#.#.###
                  .##...##.#
                  .....#.#..|]

          input5 =
            [qnb|.#..##.###...#######
                  ##.############..##.
                  .#.######.########.#
                  .###.#######.####.#.
                  #####.##.#.##.###.##
                  ..#####..#.#########
                  ####################
                  #.####....###.#.#.##
                  ##.#################
                  #####.##.###..####..
                  ..######..##.#######
                  ####.##.####...##..#
                  .#####..#.######.###
                  ##...#.##########...
                  #.##########.#######
                  .####.#.###.###.#.##
                  ....##.##.###..#####
                  .#.#.###########.###
                  #.#.#.#####.####.###
                  ###.##.####.##.#..##|]

      it "parsing" $
        Day10.parseMap input1 `shouldBe` map1

      it "visibles" $
        fmap (\a -> (a, Day10.visibleFrom map1 a)) (Set.toList $ asteroids map1)
        `shouldMatchList`
        [ ((1,0), Set.fromList [ (4,0), (0,2), (1,2), (2,2), (3,2), (4,2), (4,4) ])
        , ((4,0), Set.fromList [ (1,0), (0,2), (1,2), (2,2), (3,2), (4,2), (3,4) ])
        , ((0,2), Set.fromList [ (1,0), (4,0), (1,2), (4,3), (3,4), (4,4) ])
        , ((1,2), Set.fromList [ (1,0), (4,0), (0,2), (2,2), (4,3), (3,4), (4,4) ])
        , ((2,2), Set.fromList [ (1,0), (4,0), (1,2), (3,2), (4,3), (3,4), (4,4) ])
        , ((3,2), Set.fromList [ (1,0), (4,0), (2,2), (4,2), (4,3), (3,4), (4,4) ])
        , ((4,2), Set.fromList [ (1,0), (4,0), (3,2), (4,3), (3,4) ])
        , ((4,3), Set.fromList [ (0,2), (1,2), (2,2), (3,2), (4,2), (3,4), (4,4) ])
        , ((3,4), Set.fromList [ (4,0), (0,2), (1,2), (2,2), (3,2), (4,2), (4,3), (4,4) ])
        , ((4,4), Set.fromList [ (1,0), (0,2), (1,2), (2,2), (3,2), (4,3), (3,4) ])
        ]
      
      describe "best monitoring examples" $ do
        it "example1" $
          Day10.findBestMonitoring (Day10.parseMap input1) `shouldBe` ((3,4), 8)
        it "example2" $
          Day10.findBestMonitoring (Day10.parseMap input2) `shouldBe` ((5,8), 33)
        it "example3" $
          Day10.findBestMonitoring (Day10.parseMap input3) `shouldBe` ((1,2), 35)
        it "example4" $
          Day10.findBestMonitoring (Day10.parseMap input4) `shouldBe` ((6,3), 41)
        it "example5" $
          Day10.findBestMonitoring (Day10.parseMap input5) `shouldBe` ((11,13), 210)

      it "laser order example" $ do
        let map5 = Day10.parseMap input5
        (Day10.laserOrder (11,13) (Set.delete (11,13) $ asteroids map5 )) !! 199 `shouldBe` (8,2)
  
      