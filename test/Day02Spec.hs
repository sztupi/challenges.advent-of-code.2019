module Day02Spec
  ( spec
  )
where

import           Test.Hspec
import qualified IntCode
import qualified Data.Array                    as A

runProgram :: [Integer] -> [Integer]
runProgram =
  A.elems . fst . IntCode.run . (, []) . IntCode.mkProgram

-- brittany-disable-next-bindings
spec :: Spec
spec =
  describe "day02" $ 
    describe "examples" $ do
      it "example01" $ 
        runProgram [1,0,0,0,99] `shouldBe` [2,0,0,0,99]
      it "example02" $ 
        runProgram [2,3,0,3,99] `shouldBe` [2,3,0,6,99]
      it "example03" $ 
        runProgram [2,4,4,5,99,0] `shouldBe` [2,4,4,5,99,9801]
      it "example04" $ 
        runProgram [1,1,1,4,99,5,6,0,99] `shouldBe` [30,1,1,4,2,5,6,0,99]
      
