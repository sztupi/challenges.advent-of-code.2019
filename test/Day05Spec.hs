module Day05Spec
( spec )
where

import           Test.Hspec
import qualified IntCode
import           Data.Foldable
import qualified Data.Array                    as A

runProgram :: [Integer] -> [Integer] -> [Integer]
runProgram program input =
  toList . snd . IntCode.run . (, input) . IntCode.mkProgram $ program

-- brittany-disable-next-bindings
spec :: Spec
spec =
  describe "day05" $ 
    describe "examples" $ do
      let prog1 = [ 3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9 ]
      it "example01.0" $ 
        runProgram prog1 [0] `shouldBe` [0]
      it "example01.1" $ 
        runProgram prog1 [7] `shouldBe` [1]

      let prog2 = [ 3,3,1105,-1,9,1101,0,0,12,4,12,99,1 ]
      it "example02.0" $ 
        runProgram prog2 [0] `shouldBe` [0]
      it "example02.1" $ 
        runProgram prog2 [3] `shouldBe` [1]

      let prog3 = [ 3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006
                  , 20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105
                  , 1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105
                  , 1,46,98,99 ]
      it "example03 < 8" $
        runProgram prog3 [5] `shouldBe` [999]
      it "example03 == 8" $
        runProgram prog3 [8] `shouldBe` [1000]
      it "example03 > 8" $
        runProgram prog3 [17] `shouldBe` [1001]
