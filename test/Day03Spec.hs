module Day03Spec
  ( spec
  )
where

import           Data.Text                      ( Text )
import qualified Data.Text                     as Text
import           Test.Hspec
import qualified Day03
import           Day03                          ( WireDesc
                                                , WireDirection(..)
                                                , WireSegment(..)
                                                , Wire
                                                , Pos
                                                , SimpleWire
                                                , TimingSensitiveWire
                                                , parseWireText
                                                )
import           Data.Set                       ( Set )
import qualified Data.Set                      as Set
import qualified Data.Array                    as A
import           Test.Hspec.QuickCheck
import           Test.QuickCheck

findIntersectionDistance :: forall w . Wire w => [Text] -> Maybe Int
findIntersectionDistance [a,b] = 
  case (parseWireText a, parseWireText b) of
    (Right wa, Right wb) -> Day03.closestIntersectionDistance @w wa wb
    _ -> Nothing
findIntersectionDistance _ = Nothing

findIntersections :: Text -> Text -> Set Pos
findIntersections a b = 
  case (parseWireText a, parseWireText b) of
    (Right wa, Right wb) -> Day03.findIntersections (Day03.tracePoints wa) (Day03.tracePoints wb)
    _ -> Set.empty

wireToText :: WireDesc -> Text
wireToText = 
  Text.intercalate "," 
  . fmap (\(WireSegment dir len) -> dirToText dir <> Text.pack (show len))

dirToText :: WireDirection -> Text
dirToText WDLeft = "L"
dirToText WDRight = "R"
dirToText WDUp = "U"
dirToText WDDown = "D"

-- brittany-disable-next-bindings
spec :: Spec
spec =
  describe "day03" $ do
    parserSpecs
    traceSpecs
    examples

parserSpecs :: Spec
parserSpecs =
  describe "parser" $
    it "should parse back all wires" $
      property $ \wire -> (parseWireText . wireToText) wire === Right wire

traceSpecs :: Spec
traceSpecs =
  describe "tracing lines" $ do
    it "should trace single segment" $
      Day03.tracePoints [WireSegment WDUp 3]
      `shouldBe`
      [(0,-1), (0,-2), (0,-3)]
    it "should trace subsequent segments" $
      Day03.tracePoints [ WireSegment WDUp 3
                        , WireSegment WDRight 4
                        , WireSegment WDDown 1 ]
      `shouldBe`
      [(0,-1), (0,-2), (0,-3), (1,-3), (2,-3), (3,-3), (4,-3), (4,-2)]

examples :: Spec 
examples =
  describe "examples" $ do
    describe "intersections" $
      it "example00" $
        findIntersections "R8,U5,L5,D3" "U7,R6,D4,L4"
          `shouldBe` 
          Set.fromList [ (3, -3), (6, -5) ]

    describe "simple wires" $ do
      it "example00" $
        findIntersectionDistance @SimpleWire
          [ "R8,U5,L5,D3"
          , "U7,R6,D4,L4" ]
          `shouldBe` Just 6
      it "example01" $ 
        findIntersectionDistance @SimpleWire
          [ "R75,D30,R83,U83,L12,D49,R71,U7,L72"
          , "U62,R66,U55,R34,D71,R55,D58,R83" ]
          `shouldBe` Just 159
      it "example02" $ 
        findIntersectionDistance @SimpleWire
          [ "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
          , "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7" ]
          `shouldBe` Just 135

    describe "timing dependent wires" $ do
      it "example00" $
        findIntersectionDistance @TimingSensitiveWire
          [ "R8,U5,L5,D3"
          , "U7,R6,D4,L4" ]
          `shouldBe` Just 30
      it "example01" $ 
        findIntersectionDistance @TimingSensitiveWire
          [ "R75,D30,R83,U83,L12,D49,R71,U7,L72"
          , "U62,R66,U55,R34,D71,R55,D58,R83" ]
          `shouldBe` Just 610
      it "example02" $ 
        findIntersectionDistance @TimingSensitiveWire
          [ "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
          , "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7" ]
          `shouldBe` Just 410


instance Arbitrary WireDirection where
  arbitrary = arbitraryBoundedEnum

instance Arbitrary WireSegment where
  arbitrary = WireSegment <$> arbitrary <*> (getPositive <$> arbitrary)