module Day04Spec
  ( spec
  )
where

import           Data.Text                      ( Text )
import qualified Data.Text                     as Text
import qualified Data.List as List
import           Test.Hspec
import qualified Day04
import           Day04                          ( digits )
import           Test.Hspec.QuickCheck
import           Test.QuickCheck

-- brittany-disable-next-bindings
spec :: Spec
spec =
  describe "day04" $
    describe "digits" $ do
      it "example" $
        digits 1235 `shouldBe` [1,2,3,5]

      it "works for all non-negative ints" $
        property $ \(getNonNegative -> n :: Int) -> 
          n === List.foldl' (\acc d -> acc * 10 + d) 0 (digits n)