module Day07Spec
( spec )
where

import           Test.Hspec
import           IntCode (chainMachine, loopMachine, mkProgram)
import qualified IntCode
import qualified Data.Array                    as A
import           Data.Foldable
import qualified Data.Sequence                 as Seq

runMachine :: [Integer] -> IntCode.Machine -> [Integer]
runMachine input = toList . IntCode.runMachine input

-- brittany-disable-next-bindings
spec :: Spec
spec =
  describe "day07" $ do
    describe "single chained machinery run" $
      describe "examples" $ do
        it "example01" $ do
          let prog = mkProgram [3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0]
              machine = chainMachine
                        [ (prog, [4])
                        , (prog, [3])
                        , (prog, [2])
                        , (prog, [1])
                        , (prog, [0])
                        ]
          runMachine [0] machine `shouldBe` [43210]

        it "example02" $ do
          let prog = mkProgram
                     [ 3,23,3,24,1002,24,10,24,1002,23,-1,23,101
                     , 5,23,23,1,24,23,23,4,23,99,0,0 ]
              machine = chainMachine 
                       [ (prog, [0])
                       , (prog, [1])
                       , (prog, [2])
                       , (prog, [3])
                       , (prog, [4])
                       ]
          runMachine [0] machine `shouldBe` [54321]

        it "example03" $ do
          let prog = mkProgram
                     [ 3,31,3,32,1002,32,10,32,1001,31,-2,31
                     , 1007,31,0,33,1002,33,7,33,1,33,31,31
                     , 1,32,31,31,4,31,99,0,0,0 ]
              machine = chainMachine 
                       [ (prog, [1])
                       , (prog, [0])
                       , (prog, [4])
                       , (prog, [3])
                       , (prog, [2])
                       ]
          runMachine [0] machine `shouldBe` [65210]

    describe "looped machinery run" $
      describe "examples" $ do
        it "example01" $ do
          let prog = mkProgram [ 3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26
                               , 27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5 ]
              machine = loopMachine
                        [ (prog, [9])
                        , (prog, [8])
                        , (prog, [7])
                        , (prog, [6])
                        , (prog, [5])
                        ]
          runMachine [0] machine `shouldBe` [139629729]

        it "example02" $ do
          let prog = mkProgram
                     [ 3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005
                     , 55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55
                     , 1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6
                     , 99,0,0,0,0,10 ]
              machine = loopMachine 
                        [ (prog, [9])
                        , (prog, [7])
                        , (prog, [8])
                        , (prog, [5])
                        , (prog, [6])
                        ]
          runMachine [0] machine `shouldBe` [18216]