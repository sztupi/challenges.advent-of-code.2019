-- {-# LANGUAGE QuasiQuotes #-}
module Day11Spec
  ( spec )
where

import           Test.Hspec
import           Text.InterpolatedString.QM     ( qnb )
import qualified Day11
import qualified Data.Set as Set
import           Data.Set                       ( (\\) )

-- brittany-disable-next-bindings
spec :: Spec
spec = 
  describe "day11" $
    it "" pending